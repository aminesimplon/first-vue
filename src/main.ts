import { createApp } from 'vue'
import { router } from './rooter'
import App from './App.vue'

createApp(App)
.use(router)
.mount('#app')
