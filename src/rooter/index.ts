import { createRouter, createWebHistory } from "vue-router";
import Counter from '@/views/Counter.vue';
import Loop from '@/views/Loop.vue';
import InputModel from '@/views/InputModel.vue';
import StudentForm from '@/views/StudentForm.vue';
import NotFound from '@/views/NotFound.vue';

export const router  = createRouter({
    history: createWebHistory(),
    routes: [
        {path: '/', component: Counter},
        {path: '/loop', component: Loop},
        {path: '/input', component: InputModel},
        {path: '/form', component: StudentForm},
        { path: '/:pathMatch(.*)*', name: 'NotFound', component: NotFound }
    ]
});